# Provision infrastructure 
To begin the provisioning of the infrastructurem, you will need to install AWS CLI and create an IAM user with administrator privileges.

*Administrator permission so we wont run into permission issues while provisioning the infrastructure*

## Download and install AWS CLI
1. To download and install AWS CLI, visit [here](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

2. Configure AWS CLI
```
aws configure
```
Follow the prompt and enter the required IAM user credentals for authentication

# 

## Creating an S3 bucket for template upload
Using the appserver.template file in the root directory,

1. Open the AWS CloudFormation console and choose Create Stack.

2. In the Stack Name box, enter the stack name `planatech`.

3. Choose Upload template file, choose Browse, select the planatech.template file, and then choose Next Step.

4. On the Specify Parameters page, select I acknowledge that this template may create IAM resources, then choose Next Step on each page of the wizard until you reach the end. Choose Create.

5. After the planatech stack reaches CREATE_COMPLETE status, select it and choose the Outputs tab.

You might need to refresh a few times to update the status.

6. On the Outputs tab, record the BucketName value for later use.

## Upload template to S3 bucket

*Edit the master.yaml and update the bucket name under Resources:VPCStack:TypeProperties:TemplateURL:*

Run these commands below, and change *BucketName* to the one from the outputs tab:

```
cd Cloudformation/vpc

export BUCKET=<BucketName>

aws s3 sync ./ s3://BUCKET/infra --exclude ".git/*" --delete
```

Run this to launch the template
```
aws cloudformation create-stack --stack-name cloud-vpc --template-body file://master.yaml --parameters  file://plan-a-1.json --tags Key=Name,Value="plan-a-stack"
```


## Create cluster using EKSCTL
1. Install eksctl on your system using this [link](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html) 

2. Test that your installation was successful with the following command.

```
eksctl version
``` 

3. Create the EKS cluster in the VPC and subnet created using cluster_config.yaml file
<!-- ### Some things to change in the cluster_config.yaml -->

- Get the private subnet IDs from the VPC created using this command. 
```
aws ec2 describe-subnets 
``` 
It will look something like this `cloud-vpc-VPCStack-1GLJ8UIX69MPK-private-subnet-(AZ1)` and `cloud-vpc-VPCStack-1GLJ8UIX69MPK-private-subnet-(AZ2)` 


> Make sure to add the SubnetIDs to the cluster_config.yaml file before provisioning the cluster

You can provision the EKS cluster using this command
```
eksctl create cluster --config-file ./cluster_config.yaml
```
Confirm that EKS cluster is created
```
eksctl get cluster
```

Connect to the EKS cluster by getting the kubeconfig file
```
eksctl utils write-kubeconfig --cluster chidi-plan-a-cluster
```

Confirm connection to the cluster when you view cluster resources by running
```
kubectl get pods --all-namespaces
```